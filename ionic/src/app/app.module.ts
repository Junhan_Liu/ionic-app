import { ErrorHandler, NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { SplashScreen } from '@ionic-native/splash-screen'
import { StatusBar } from '@ionic-native/status-bar'
import axios from 'axios'
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular'
import { MyApp } from './app.component'
import { GalleryPage } from '../pages/gallery/gallery'
import { HomePage } from '../pages/home/home'
import { InformationPage } from '../pages/information/information'
import { ListPage } from '../pages/list/list'
import { LoginPage } from '../pages/login/login'
import { RegisterPage } from '../pages/register/register'
import { SettingPage } from '../pages/setting/setting'
import { NavigationPage } from '../pages/navigation/navigation'
import { Chart1Page } from '../pages/chart1/chart1'

//remote server; 
//if running in localhost, change it to http://localhost/... OR http://localhost:8100/...;
axios.defaults.baseURL = 'http://jimjim.gq/api'

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    RegisterPage,
    GalleryPage,
    InformationPage,
    SettingPage,
    NavigationPage,
    Chart1Page
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    RegisterPage,
    LoginPage,
    GalleryPage,
    InformationPage,
    SettingPage,
    NavigationPage,
    Chart1Page
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
  ]
})
export class AppModule { }
