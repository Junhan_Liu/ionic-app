import { Component } from '@angular/core'
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular'
import { LoginPage } from '../login/login'
import {App} from 'ionic-angular'

@IonicPage()
@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html',
})
export class GalleryPage {

  lists: any[] = []

  constructor (public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
    private app: App) {

    Array(10).fill(null).map((_, index) => {
      this.lists.push({
        src: `http://source.unsplash.com/random/800x${599 + index}`
      })
    })

  }

  ionViewDidLoad () {
    console.log('ionViewDidLoad GalleryPage');
  }

  
}
