import { Component } from '@angular/core'
import axios from 'axios'
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular'
import { LoadingController } from 'ionic-angular'
import { RegisterPage } from '../register/register'
import { MenuController } from 'ionic-angular/index'
import { SettingPage } from '../setting/setting'

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor (public navCtrl: NavController, public loadingCtrl: LoadingController, public alertCtrl: AlertController,
    private menuCtrl: MenuController) { }

  email: string = ''
  password: string = ''

  //disable swipe gesture in the page;
  ionViewDidEnter(){
    this.menuCtrl.swipeEnable(false);
  }
  //enable swipe gesture when leave this page;
  //otherwise, other pages will be disable too;
  ionViewWillLeave(){
    this.menuCtrl.swipeEnable(true);
  }

  ionViewDidLoad () {
    console.log('ionViewDidLoad LoginPage');
  }

  toSignIn () {
    this.navCtrl.push(RegisterPage)
  }

  async login () {

    if (this.email === '' || this.password === '') {
      this.alertCtrl.create({
        buttons: ['ok'],
        title: 'Please fill all fieds'
      }).present()
      return
    }

    const loader = this.loadingCtrl.create({ content: 'Please wait' })
    loader.present()

    axios.post('/login.php', {
      email: this.email,
      password: this.password
    })
      .then((res) => {
        loader.dismiss()
        if (!res.data.current_user) {
          const message = res.data.message
          this.alertCtrl.create({
            buttons: ['ok'],
            title: message
          }).present()
          return
        }
        //direct to Setting Page(main page) after login;
        this.navCtrl.setRoot(SettingPage)
        localStorage.setItem('isLogin', 'true')
        localStorage.setItem('email', this.email)
      })
  }
}
