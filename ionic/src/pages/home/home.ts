import { Component } from '@angular/core'
import { AlertController, NavController } from 'ionic-angular'
import { LogoutProvider } from '../../providers/logout/logout'
import { LoginPage } from '../login/login'
//import top App; 
import { App } from 'ionic-angular' 

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor (public navCtrl: NavController, public alertCtrl: AlertController, private app: App) {

  }
}
