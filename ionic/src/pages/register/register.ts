import { Component } from '@angular/core'
import axios from 'axios'
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular'
import { LoginPage } from '../login/login'

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  constructor (public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  email: string = ''
  password: string = ''
  rePassword: string = ''

  ionViewDidLoad () {
    console.log('ionViewDidLoad RegisterPage');
  }

  signUp () {
    this.validate(() => {
      axios.post('/register.php', {
        email: this.email,
        password: this.password
      })
        .then(res => {
          if (res.data.message) {
            this.alertCtrl.create({
              title: 'Reigster failed',
              subTitle: res.data.message,
              buttons: ['ok']
            }).present()
            return
          }

          this.alertCtrl.create({
            title: 'Success',
            message: 'Press ok and login',
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  this.navCtrl.setRoot(LoginPage)
                }
              }
            ]
          }).present()
        })
    })
  }

  validate (cb: any) {
    if (isEmpty(this.password) || isEmpty(this.email) || isEmpty(this.rePassword)) {
      const alert = this.alertCtrl.create({
        title: 'Validate failed',
        subTitle: 'All fields are required',
        buttons: ['ok']
      })
      alert.present()
      return
    }

    if (this.password !== this.rePassword) {
      const alert = this.alertCtrl.create({
        title: 'Validate failed',
        subTitle: 'password must be same',
        buttons: ['ok']
      })
      alert.present()
      return
    }

    if (!this.email.includes('@')) {
      const alert = this.alertCtrl.create({
        title: 'Validate failed',
        subTitle: 'Email is not correct',
        buttons: ['ok']
      })
      alert.present()
      return
    }

    cb && cb()
  }

}

function isEmpty (val: string) {
  return val.trim() === ''
}
