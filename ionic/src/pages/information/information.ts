import { Component } from '@angular/core'
import axios from 'axios'
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular'
import { LoginPage } from '../login/login'
import {App} from 'ionic-angular'

@IonicPage()
@Component({
  selector: 'page-information',
  templateUrl: 'information.html',
})
export class InformationPage {

  constructor (public navCtrl: NavController, public alertCtrl: AlertController, private app: App) {
    this.fetchInfomation()
  }

  id = ''
  email = ''

  fetchInfomation () {
    const email = localStorage.getItem('email')
    axios.post('/info.php', { email })
      .then(res => {
        const {
          id,
          email,
        } = res.data.data

        this.id = id
        this.email = email
      })
  }

  handleLogout () {
    this.showConfirm()
  }

  showConfirm () {
    let confirm = this.alertCtrl.create({
      title: 'Confirm logout?',
      message: 'You will exit and need login again?',
      buttons: [
        {
          text: 'Stay',
          handler: () => {
            console.log('Disagree clicked')
          }
        },
        {
          text: 'Agree',
          handler: () => {
            localStorage.clear()
            //this.navCtrl.setRoot(LoginPage)
            /*
            this.app.getRootNav().push(), will hide tabs in child component
            xxx.setRoot(), will hide tabs and set root page from top level;
            */
            this.app.getRootNav().setRoot(LoginPage)
          }
        }
      ]
    })
    confirm.present()
  }

}
