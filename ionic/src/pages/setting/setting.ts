import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { InformationPage } from '../information/information';
import { ListPage } from '../list/list';
import { NavigationPage } from '../navigation/navigation'


@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  tab1root:any= NavigationPage;
  tab2root:any= ListPage;
  tab3root:any= InformationPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
  }

}
